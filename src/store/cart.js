import axios from 'axios'
// var baseUrl = '/api/';
// if (process.env.NODE_ENV === "production") {
//     baseUrl = process.env.BACKEND_URL;
// }
var baseUrl = 'https://quasar-shop-backend.herokuapp.com/';

export default{
    state:{
        items: []
    },
    mutations:{
        SET_ITEMS: (state, payload) => {
            state.items = payload;
        },
        UNSET_ITEM: (state, payload) => {
            state.items.splice(payload, 1);
        },
        UNSET_ALL_ITEMS: (state) => {
            state.items = [];
        },
    },
    actions: {
        async REMOVE_ITEM({commit, getters}, payload){
            return await axios({
                method: "DELETE",
                url: baseUrl + `customers/cart/delete/one`,
                headers: {Authorization: getters.getUser.token},
                data: {
                    id: payload,
                    // user_id: getters.getUser.user_id
                }
            })
            .then((e) => {
                console.log(e); 
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return error.response;
            })
        },
        async REMOVE_ALL_ITEMS({commit, getters}){
            return await axios({
                method: "DELETE",
                url: baseUrl + `customers/cart/delete/all`,
                headers: {Authorization: getters.getUser.token},
                data: { user_id: getters.getUser.user_id }
            })
            .then((e) => {
                console.log(e); 
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return error.response;
            })
        },
        async ADD_CUSTOMER_PURCHASE({commit, getters}, payload) {
            payload.user_id = getters.getUser.user_id
            return await axios({
                method: "POST",
                url: baseUrl + 'customers/add/product',
                headers: {Authorization: getters.getUser.token},
                data: payload
            })
            .then(async (e) => {
                console.log(e.data); 
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return error.response;
            })
        },
        async GET_ITEMS({commit, getters}) {
            return await axios({
                method: "GET",
                url: baseUrl + `customers/cart/items/${getters.getUser.user_id}`,
                headers: { Authorization: getters.getUser.token },
            })
            .then(async (e) => {
                console.log(e.data)
                await commit('SET_ITEMS', e.data);
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return 'error';
            })
        },
        async ADD_ITEM({commit, getters}, payload) {
            payload.user_id = getters.getUser.user_id
            return await axios({
                method: "POST",
                url: baseUrl + 'customers/add/item/cart',
                headers: {Authorization: getters.getUser.token},
                data: payload
            })
            .then(async (e) => {
                console.log(e.data)
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return 'error';
            })
        },
        async UPDATE_ITEM({commit, getters}, payload) {
            // payload.user_id = getters.getUser.user_id
            return await axios({
                method: "PATCH",
                url: baseUrl + `customers/update/item/cart/${payload.id}`,
                headers: {Authorization: getters.getUser.token},
                data: payload
            })
            .then(async (e) => {
                console.log(e.data)
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return 'error';
            })
        },
     
       
    },
    getters:{
        getItems: state => state.items,

    }
}