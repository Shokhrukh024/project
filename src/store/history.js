import axios from 'axios'
// var baseUrl = '/api/';
// if (process.env.NODE_ENV === "production") {
//     baseUrl = process.env.BACKEND_URL;
// }

var baseUrl = 'https://quasar-shop-backend.herokuapp.com/';

export default{
    state:{
        history_products: [],
    },
    mutations:{
        SET_HISTORY_PRODUCTS: (state, payload) => {
            state.history_products = payload
        },
        // SET_NEXT_PAGE_HISTORY: (state, payload) => {
        //     console.log(payload);
        //    state.history_products.push(payload);
        // }
    },
    actions: {
        async GET_HISTORY_PRODUCTS({commit, getters}, payload) {
            return await axios({
                method: "GET",
                url: baseUrl + `customers/products/history/${payload}/${getters.getUser.user_id}`,
                headers: { Authorization: getters.getUser.token },
            })
            .then(async (e) => {
                await commit('SET_HISTORY_PRODUCTS', e.data);
            //   return e;
            })
            .catch((error) => {
                console.log(error);
                return 'error';
            })
        },
        async EDIT_HISTORY_PRODUCT({commit, getters}, payload){
            return await axios({
                method: "PATCH",
                url: baseUrl + `customers/edit/history/product/${payload.id}`,
                headers: { Authorization: getters.getUser.token }, //user_id: getters.getUser.user_id
                data: {
                    date: payload.date,
                    name: payload.name,
                    customer_name: payload.customer_name,
                    amount: payload.amount,
                    measure: payload.measure,
                    buyPrice: payload.buyPrice,
                }
            })
            .then((e) => {
                console.log(e); 
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return error.response;
            })
        },
        async DELETE_HISTORY_PRODUCT({commit, getters}, payload){
            return await axios({
                method: "DELETE",
                url: baseUrl + `customers/delete/history/product/${payload.id}`,
                headers: { Authorization: getters.getUser.token }, //user_id: getters.getUser.user_id
            })
            .then((e) => {
                console.log(e); 
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return error.response;
            })
        },
    //     async GET_NEXT_PAGE_HISTORY({commit, getters}, payload) {
    //       return await axios({
    //           method: "GET",
    //           url: baseUrl + `customers/products/history/${payload}`,
                // headers: {Authorization: getters.getUser.token, user_id: getters.getUser.user_id},
    //       })
    //       .then(async (e) => {
    //           console.log(e.data);
    //           await commit('SET_HISTORY_PRODUCTS', e.data);
    //       //   return e;
    //       })
    //       .catch((error) => {
    //           console.log(error);
    //           return 'error';
    //       })
    //   },
       
    },
    getters:{
        getHistoryProducts: state => state.history_products,
    }
}