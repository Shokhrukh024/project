import axios from 'axios'
// var baseUrl = '/api/';
// if (process.env.NODE_ENV === "production") {
//     baseUrl = process.env.BACKEND_URL;
// }

var baseUrl = 'https://quasar-shop-backend.herokuapp.com/';

export default{
    state:{
        todos: []
    },
    mutations:{
        SET_TODOS: (state, payload) => {
            state.todos = payload;
        },
        updateTask (state, title) {
            state.task_item.title = title
        }
    },
    actions: {
        async GET_TODOS({commit, getters}, payload) {
            return await axios({
                method: "GET",
                url: baseUrl + `todos/get/all/${getters.getUser.user_id}`,
                headers: { Authorization: getters.getUser.token },
            })
            .then(async (e) => {
                console.log(e.data)
                await commit('SET_TODOS', e.data);
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return 'error';
            })
        },
        async ADD_TODOS({commit, getters}, payload) {
            return await axios({
                method: "POST",
                url: baseUrl + 'todos/add/todos',
                headers: {Authorization: getters.getUser.token},
                data: {
                    todos: payload,
                    user_id: getters.getUser.user_id
                }
            })
            .then(async (e) => {
                console.log(e.data)
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return 'error';
            })
        },
        // async UPDATE_TODO({commit, getters}, payload) {
        //     payload.user_id = getters.getUser.user_id
        //     return await axios({
        //         method: "PATCH",
        //         url: baseUrl + `todos/update/todo/${payload.id}`,
        //         headers: { Authorization: getters.getUser.token },
        //         data: payload
        //     })
        //     .then(async (e) => {
        //         console.log(e.data)
        //         return e.data;
        //     })
        //     .catch((error) => {
        //         console.log(error);
        //         return 'error';
        //     })
        // },
        // async REMOVE_TODO({commit, getters}, payload){
        //     return await axios({
        //         method: "DELETE",
        //         url: baseUrl + `todos/delete/one`,
        //         headers: {Authorization: getters.getUser.token },
        //         data: {
        //             id: payload,
        //             user_id: getters.getUser.user_id
        //         }
        //     })
        //     .then((e) => {
        //         console.log(e); 
        //         return e.data;
        //     })
        //     .catch((error) => {
        //         console.log(error);
        //         return error.response;
        //     })
        // },
       
    },
    getters:{
        getTodos: state => state.todos,
    }
}