import axios from 'axios'
// var baseUrl = '/api/';
// if (process.env.NODE_ENV === "production") {
//     baseUrl = process.env.BACKEND_URL;
// }

var baseUrl = 'https://quasar-shop-backend.herokuapp.com/';

export default{
    state:{
        customers: [],
    },
    mutations:{
        SET_CUSTOMERS: (state, payload) => {
            state.customers = payload
            // state.customers.forEach((element, index) => {
            //     element.index = index + 1
            // });
        },
        
    },
    actions: {
        async ADD_CUSTOMER({commit, getters}, payload) {
            payload.user_id = getters.getUser.user_id
            return await axios({
                method: "POST",
                url: baseUrl + 'customers',
                headers: {Authorization: getters.getUser.token},
                data: payload
            })
            .then((e) => {
                console.log(e); 
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return error.response;
            })
        },
        async EDIT_CUSTOMER({commit, getters}, payload){
            return await axios({
                method: "PATCH",
                url: baseUrl + `customers/${payload.id}`,
                headers: { Authorization: getters.getUser.token },
                data: {
                    name: payload.name,
                    phone: payload.phone,
                    companyName: payload.companyName,
                    // user_id: getters.getUser.user_id
                }
            })
            .then((e) => {
                console.log(e); 
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return error.response;
            })
        },
        async DELETE_CUSTOMER({commit, getters}, payload){
            return await axios({
                method: "DELETE",
                url: baseUrl + `customers/${payload.id}`,
                headers: { Authorization: getters.getUser.token },//user_id: getters.getUser.user_id
            })
            .then((e) => {
                console.log(e); 
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return error.response;
            })
        },
        async ADD_CUSTOMER_PRODUCT({commit, getters}, payload) {
            return await axios({
                method: "POST",
                url: baseUrl + 'customers/' + payload.id + '/add/product',
                headers: { Authorization: getters.getUser.token },
                data: {
                    productArr: payload,
                    user_id: getters.getUser.user_id
                }
            })
            .then((e) => {
                console.log(e); 
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return error.response;
            })
        },
        async DECREASE_AMOUNT_OF_PRODUCTS({commit, getters}, payload) {
            // payload.user_id = getters.getUser.user_id
            return await axios({
                method: "POST",
                url: baseUrl + 'customers/decrease/product',
                headers: {Authorization: getters.getUser.token},
                data: payload
            })
            .then((e) => {
                console.log(e); 
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return error.response;
            })
        },
        async EDIT_PRODUCT_OF_CUSTOMER({commit, getters}, payload){
            return await axios({
                method: "PATCH",
                url: baseUrl + `customers/${payload.id}/product/${payload.pid}`,
                headers: {Authorization: getters.getUser.token}, //user_id: getters.getUser.user_id
                data: payload
            })
            .then((e) => {
                console.log(e); 
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return error.response;
            })
        },
        async RETURN_PRODUCT_OF_CUSTOMER({commit, getters}, payload){
            // payload.user_id = getters.getUser.user_id
            return await axios({
                method: "POST",
                url: baseUrl + `customers/${payload.cid}/add/product/return/${payload.pid}`,
                headers: {Authorization: getters.getUser.token},
                data: payload
            })
            .then((e) => {
                console.log(e); 
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return error.response;
            })
        },
        async DELETE_PRODUCT_OF_CUSTOMERS({commit, getters}, payload){
            return await axios({
                method: "DELETE",
                url: baseUrl + `customers/${payload.id}/product/${payload.pid}`,
                headers: {Authorization: getters.getUser.token}, // user_id: getters.getUser.user_id
            })
            .then((e) => {
                console.log(e); 
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return error.response;
            })
        },
        async GET_CUSTOMERS({commit, getters}, payload) {
            return await axios({
                method: "GET",
                url: baseUrl + `customers/get/all/${payload}/${getters.getUser.user_id}`,
                headers: { Authorization: getters.getUser.token },
            })
            .then(async (e) => {
                await commit('SET_CUSTOMERS', e.data);
            //   return e;
            })
            .catch((error) => {
                console.log(error);
                return 'error';
            })
        },
        
       
    },
    getters:{
        getCustomers: state => state.customers,
    }
}