import axios from 'axios'
// var baseUrl = '/api/';
// if (process.env.NODE_ENV === "production") {
//     baseUrl = process.env.BACKEND_URL;
// }

var baseUrl = 'https://quasar-shop-backend.herokuapp.com/';

export default{
    state:{
        producers: [],
        return_products: []
    },
    mutations:{
        SET_PRODUCERS: (state, payload) => {
            state.producers = payload
            // state.producers.forEach((element, index) => {
            //     element.index = index + 1
            // });
        },
        SET_RETURN_PRODUCTS: (state, payload) => {
            state.return_products = payload
            state.return_products.forEach((element, index) => {
                element.index = index + 1
            });
        },
    },
    actions: {
        async ADD_PRODUCER_PRODUCT_DETAIL({commit, getters}, payload) {
            return await axios({
                method: "POST",
                url: baseUrl + `producers/product/${payload.product_id}/add/detail/`,
                headers: {Authorization: getters.getUser.token},
                data: {
                    productDetailArr: {
                        user_id: getters.getUser.user_id,
                        date: payload.date,
                        amount: payload.amount, 
                        amountLeft: payload.amount,
                        buyPrice: payload.buyPrice, 
                        // sellPrice: payload.sellPrice, 
                        payed: payload.payed,
                        unPayed: payload.unPayed,
                        about: payload.about
                    },
                }
            })
            .then((e) => {
                console.log(e); 
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return error.response;
            })
        },
        async ADD_PRODUCER({commit, getters}, payload) {
            payload.user_id = getters.getUser.user_id
            return await axios({
                method: "POST",
                url: baseUrl + `producers`,
                headers: { Authorization: getters.getUser.token },
                data: payload
            })
            .then((e) => {
                console.log(e); 
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return error.response;
            })
        },
        async EDIT_PRODUCER({commit, getters}, payload){
            return await axios({
                method: "PATCH",
                url: baseUrl + `producers/${payload.id}`,
                headers: { Authorization: getters.getUser.token }, //user_id: getters.getUser.user_id
                data: {
                    name: payload.name,
                    phone: payload.phone,
                    companyName: payload.companyName
                }
            })
            .then((e) => {
                console.log(e); 
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return error.response;
            })
        },
        async EDIT_RETURNED_PRODUCT({commit, getters}, payload){
            return await axios({
                method: "PATCH",
                url: baseUrl + `customers/edit/return/product/${payload.id}`,
                headers: { Authorization: getters.getUser.token }, //user_id: getters.getUser.user_id
                data: {
                    amount: payload.amount,
                    measure: payload.measure,
                    buyPrice: payload.buyPrice,
                    returnReason: payload.returnReason,
                }
            })
            .then((e) => {
                console.log(e); 
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return error.response;
            })
        },
        async DELETE_PRODUCER({commit, getters}, payload){
            return await axios({
                method: "DELETE",
                url: baseUrl + `producers/${payload.id}`,
                headers: { Authorization: getters.getUser.token }, //user_id: getters.getUser.user_id
                data: {
                    user_id: getters.getUser.user_id
                }
            })
            .then((e) => {
                console.log(e); 
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return error.response;
            })
        },
        async ADD_PRODUCER_PRODUCT({commit, getters}, payload) {
            return await axios({
                method: "POST",
                url: baseUrl + 'producers/' + payload.id + '/add/product/',
                headers: { Authorization: getters.getUser.token },
                data: {
                    productArr: payload,
                    user_id: getters.getUser.user_id
                }
            })
            .then((e) => {
                console.log(e); 
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return error.response;
            })
        },
        async GET_PRODUCERS({commit, getters}, payload) {
            return await axios({
                method: "GET",
                url: baseUrl + `producers/get/all/${payload}/${getters.getUser.user_id}`,
                headers: { Authorization: getters.getUser.token },
            })
            .then(async (e) => {
                await commit('SET_PRODUCERS', e.data);
            //   return e;
            })
            .catch((error) => {
                console.log(error);
                return 'error';
            })
        },
        async GET_RETURN_PRODUCTS({commit, getters}, payload) {
            return await axios({
                method: "GET",
                url: baseUrl + `customers/get/all/return/products/${getters.getUser.user_id}`,
                headers: { Authorization: getters.getUser.token },
            })
            .then(async (e) => {
                await commit('SET_RETURN_PRODUCTS', e.data);
            //   return e;
            })
            .catch((error) => {
                console.log(error);
                return 'error';
            })
        },
        async DELETE_RETURNED_PRODUCT({commit, getters}, payload){
            return await axios({
                method: "DELETE",
                url: baseUrl + `customers/delete/return/product/${payload.id}`,
                headers: {Authorization: getters.getUser.token}, //user_id: getters.getUser.user_id
            })
            .then((e) => {
                console.log(e); 
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return error.response;
            })
        },
    },
    getters:{
        getProducers: state => state.producers,
        getReturnProducts: state => state.return_products
    }
}