import axios from 'axios'
// var baseUrl = '/api/';
// if (process.env.NODE_ENV === "production") {
//     baseUrl = process.env.BACKEND_URL;
// }

var baseUrl = 'https://quasar-shop-backend.herokuapp.com/';

export default{
    state:{
        products_of_producers: [],
        product_detail_of_producer: [],
        product_detail_of_producer_history: [],
        products_of_customers: [],
        all_products: [],
    },
    mutations:{
        SET_PRODUCTS_DETAIL_OF_PRODUCER: (state, payload) => {
            state.product_detail_of_producer = payload
            // state.product_detail_of_producer.forEach((element, index) => {
            //     element.index = index + 1
            // });
        },
        SET_PRODUCTS_DETAIL_OF_PRODUCER_HISTORY: (state, payload) => {
            state.product_detail_of_producer_history = payload
            state.product_detail_of_producer_history.forEach((element, index) => {
                element.index = index + 1
            });
        },
        SET_PRODUCTS_OF_PRODUCERS: (state, payload) => {
            state.products_of_producers = payload
            // state.products_of_producers.forEach((element, index) => {
            //     element.index = index + 1
            // });
        },
        // SET_ALL_PRODUCTS: (state, payload) => {
        //     state.all_products = payload
        //     // state.products_of_producers.forEach((element, index) => {
        //     //     element.index = index + 1
        //     // });
        // },
        SET_PRODUCTS_OF_CUSTOMERS: (state, payload) => {
            state.products_of_customers = payload
            state.products_of_customers.forEach((element, index) => {
                element.index = index + 1
            });
        },
        SET_ALL_PRODUCER_PRODUCTS: (state, payload) => {
            state.all_products = payload
            // state.all_products.forEach((element, index) => {
            //     element.index = index + 1
            // });
        },
    },
    actions: {          
        async GET_ALL_PRODUCER_PRODUCTS({commit, getters}, payload) {
            return await axios({
                method: "GET",
                url: baseUrl + `producers/products/all/${payload}/${getters.getUser.user_id}`,
                headers: {Authorization: getters.getUser.token},
            })
            .then(async (e) => {
                await commit('SET_ALL_PRODUCER_PRODUCTS', e.data);
            //   return e;
            })
            .catch((error) => {
                console.log(error);
                return 'error';
            })
        },
        
        async FILTER_PRODUCTS_OF_PRODUCERS_BY_NAME({commit, getters}, payload) {
            return await axios({
                method: "GET",
                url: baseUrl + `producers/filter/by/${payload}/${getters.getUser.user_id}`,
                headers: {Authorization: getters.getUser.token},
            })
            .then(async (e) => {
                // await commit('SET_ALL_PRODUCTS', e.data);
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return 'error';
            })
        },
        // async GET_ALL_PRODUCTS({commit, getters}, payload) {
        //     return await axios({
        //         method: "GET",
        //         url: baseUrl + `products`,
        //         headers: {Authorization: getters.getUser.token, user_id: getters.getUser.user_id},
        //     })
        //     .then(async (e) => {
        //         await commit('SET_ALL_PRODUCTS', e.data);
        //     //   return e;
        //     })
        //     .catch((error) => {
        //         console.log(error);
        //         return 'error';
        //     })
        // },
        async GET_PRODUCTS_OF_PRODUCERS({commit, getters}, payload) {
            console.log(payload)
            return await axios({
                method: "GET",
                url: baseUrl + `producers/${payload.id}/products/${payload.page}`,
                headers: {Authorization: getters.getUser.token},
            })
            .then(async (e) => {
                await commit('SET_PRODUCTS_OF_PRODUCERS', e.data);
            //   return e;
            })
            .catch((error) => {
                console.log(error);
                return 'error';
            })
        },
        async GET_PRODUCTS_DETAIL_OF_PRODUCER({commit, getters}, payload) {
            return await axios({
                method: "GET",
                url: baseUrl + `producers/${payload.id}/product/detail/${payload.page}`,
                headers: {Authorization: getters.getUser.token},
            })
            .then(async (e) => {
                await commit('SET_PRODUCTS_DETAIL_OF_PRODUCER', e.data);
            //   return e;
            })
            .catch((error) => {
                console.log(error);
                return 'error';
            })
        },
        async GET_PRODUCTS_DETAIL_OF_PRODUCER_TOTAL_AMOUNT({commit, getters}, payload) {
            return await axios({
                method: "GET",
                url: baseUrl + `producers/${payload.id}/product/detail/calculate/total`,
                headers: {Authorization: getters.getUser.token},
            })
            .then(async (e) => {
                console.log(e);
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return 'error';
            })
        },
        async GET_PRODUCTS_DETAIL_OF_PRODUCER_TOTAL_PAYED({commit, getters}, payload) {
            return await axios({
                method: "GET",
                url: baseUrl + `producers/${payload.id}/product/detail/calculate/payed`,
                headers: {Authorization: getters.getUser.token},
            })
            .then(async (e) => {
                console.log(e);
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return 'error';
            })
        },
        async GET_PRODUCTS_DETAIL_OF_PRODUCER_TOTAL_UNPAYED({commit, getters}, payload) {
            return await axios({
                method: "GET",
                url: baseUrl + `producers/${payload.id}/product/detail/calculate/unpayed`,
                headers: {Authorization: getters.getUser.token},
            })
            .then(async (e) => {
                console.log(e);
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return 'error';
            })
        },
        async GET_PRODUCTS_DETAIL_OF_PRODUCER_HISTORY({commit, getters}, payload) {
            return await axios({
                method: "GET",
                url: baseUrl + `producers/${payload.product_id}/product/detail/history/${payload.page}`,
                headers: {Authorization: getters.getUser.token},
            })
            .then(async (e) => {
                await commit('SET_PRODUCTS_DETAIL_OF_PRODUCER_HISTORY', e.data);
            //   return e;
            })
            .catch((error) => {
                console.log(error);
                return 'error';
            })
        },
        async GET_PRODUCTS_OF_CUSTOMERS({commit, getters}, payload) {
            return await axios({
                method: "GET",
                url: baseUrl + `customers/${payload}/products`,
                headers: {Authorization: getters.getUser.token, user_id: getters.getUser.user_id},
            })
            .then(async (e) => {
                await commit('SET_PRODUCTS_OF_CUSTOMERS', e.data);
            //   return e;
            })
            .catch((error) => {
                console.log(error);
                return 'error';
            })
        },
        async EDIT_PRODUCT({commit, getters}, payload){
            return await axios({
                method: "PATCH",
                url: baseUrl + `producers/${payload.id}/product/${payload.pid}`,
                headers: {Authorization: getters.getUser.token},
                data: {
                    barcode: payload.barcode,
                    name: payload.name,
                    amount: payload.amount,
                    measure: payload.measure,
                    // buyPrice: payload.buyPrice,
                    sellPrice: payload.sellPrice,
                    description: payload.description,
                }
            })
            .then((e) => {
                console.log(e); 
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return error.response;
            })
        },
        async EDIT_PRODUCT_DETAIL({commit, getters}, payload){
            return await axios({
                method: "PATCH",
                url: baseUrl + `producers/product/detail/${payload.did}`,
                headers: {Authorization: getters.getUser.token},
                data: {
                    date: payload.date,
                    amount: payload.amount,
                    amountLeft: payload.amountLeft,
                    buyPrice: payload.buyPrice,
                    // sellPrice: payload.sellPrice,
                    payed: payload.payed,
                    unPayed: payload.unPayed,
                    about: payload.about,
                }
            })
            .then((e) => {
                console.log(e); 
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return error.response;
            })
        },
        async DELETE_PRODUCT({commit, getters}, payload){
            return await axios({
                method: "DELETE",
                url: baseUrl + `producers/${payload.id}/product/${payload.pid}`,
                headers: {Authorization: getters.getUser.token},
                data: {
                    user_id: getters.getUser.user_id
                }
            })
            .then((e) => {
                console.log(e); 
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return error.response;
            })
        },
        async DELETE_PRODUCT_DETAIL({commit, getters}, payload){
            console.log(payload);
            return await axios({
                method: "DELETE",
                url: baseUrl + `producers/product/${payload.pid}/detail/${payload.did}`,
                headers: {Authorization: getters.getUser.token},
            })
            .then((e) => {
                console.log(e); 
                return e.data;
            })
            .catch((error) => {
                console.log(error);
                return error.response;
            })
        },

       
    },
    getters:{
        getProductsOfProducers: state => state.products_of_producers,
        getProductDetailOfProducer: state => state.product_detail_of_producer,
        getProductDetailOfProducerHistory: state => state.product_detail_of_producer_history,
        getProductsOfCustomers: state => state.products_of_customers,
        getAllProducts: state => state.all_products,
    }
}