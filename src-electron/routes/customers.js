const express = require('express')
const router = express.Router()
const Customer = require('../models/customer')
const Product_producer = require('../models/product-producer')
const Product_customer = require('../models/product-customer')
const Product_producer_detail = require('../models/product-producer-detail')
const Product_customer_return = require('../models/product-customer-return')
const Product_default_customer = require('../models/product-default-customer')
const Cart = require('../models/cart')



//Getting all customers
router.get('/', async (req, res) => {
    try{
        const customer = await Customer.find();
        res.json(customer);
    }catch(error){
        res.status(500).json({message: error.message});
    }
})
//Getting all items in cart
router.get('/cart/items', async (req, res) => {
    try{
        const cart = await Cart.find();
        res.json(cart);
    }catch(error){
        res.status(500).json({message: error.message});
    }
})
//Getting customer's products
router.get('/:id/products', async (req, res) => {
    try{
        await Customer.findById(req.params.id)
        .populate('productArr')
        .exec(async function(error, element){
            if (error) return res.status(400).json({message: error.message})

            res.json(element.productArr);
        })
    }catch(error){
        res.status(500).json({message: error.message})
    }
})
//Getting all returned products
router.get('/get/all/return/products', async (req, res) => {
    try{
        const return_products = await Product_customer_return.find();
        res.json(return_products);
    }catch(error){
        res.status(500).json({message: error.message});
    }
})
//Getting history products by page
router.get('/products/history/:id', async (req, res) => {
    try{
        // var aggregateQuery = Product_default_customer.aggregate();
        // Product_default_customer.aggregatePaginate(aggregateQuery, { page: req.params.id, limit: 10 }, function(err, result) {
        //     if (err) {
        //         console.err(err);
        //     } else {
        //         res.json(result);
        //     }
        // });

        var query = {};
        var options = {
            page: req.params.id,
            limit: 11,
        };

        Product_default_customer.paginate(query, options).then(function (result) {
            res.json(result);
        }).catch(function(error) {
            console.log(error); 
        })
    }catch(error){
        res.status(500).json({message: error.message});
    }
})

//Getting one
router.get('/:id', getCustomer, (req, res) => {
    res.json(res.customer)
})
//Creating one
router.post('/', async (req, res) => {
    const customer = new Customer({
        name: req.body.name,
        phone: req.body.phone,
        companyName: req.body.companyName,
    })
    try {
        const newCustomer = await customer.save()
        res.status(201).json(newCustomer)
    } catch (error) {
        res.status(400).json({message: error.message})
    }
})
//Add item to cart
router.post('/add/item/cart', async (req, res) => {
    try {
        // let cart_items = await Cart.findOne({});
        // let cartChanged = null;
        // if(cart_items == null){
            let productNew = await new Cart(req.body)
            cartChanged = await productNew.save()
        // }else{
            // await cart_items.items.push(req.body);
            // cartChanged = await cart_items.save();
        // }
        res.status(201).json(cartChanged)
    } catch (error) {
        res.status(400).json({message: error.message})
    }
})
// Add product to customer
router.post('/:id/add/product', async (req, res) => {
    try {
        let productNew = await new Product_customer(req.body.productArr)
        await Product_producer.findOne({_id: req.body.productArr.product_id})
        .populate({
            path: 'productDetailArr',
        })
        .exec(async function (error, element) {
            if (error) return res.status(400).json({message: error.message})
            let amount = productNew.amount
            for(let i = 0; i < element.productDetailArr.length; i++){
                let producer_product_detail = await Product_producer_detail.findById(element.productDetailArr[i].id)
                if(productNew.amount <= element.productDetailArr[i].amountLeft){
                    producer_product_detail.amountLeft = producer_product_detail.amountLeft - amount
                    await producer_product_detail.save();
                    break;
                }else{
                    amount = amount - producer_product_detail.amountLeft
                    producer_product_detail.amountLeft = 0
                    await producer_product_detail.save();
                }
            }
            await element.save();//?
        });

        await Customer.findById(req.params.id)
        .populate('productArr')
        .exec(async function (error, element) {
            if (error) return res.status(400).json({message: error.message})
            // product.amount = product.amount - req.body.productArr.amount
            // await product.save();
            await productNew.save();
            await element.productArr.push(productNew);
            const updatedCustomer = await element.save();
            res.json(updatedCustomer)
        });
    } catch (error) {
        res.status(400).json({message: error.message})
    }
})

// Add default customer product
router.post('/add/product', async (req, res) => {
    try {
        //Product_default_customer
        // let products_default = await Product_default_customer.findOne({});
        let productsChanged = null;
        // if(products_default == null){
            let productNew = await new Product_default_customer(req.body)
            productsChanged = await productNew.save();
            console.log(productNew);
        // }else{
        //     await products_default.products.push(req.body);
        //     productsChanged = await products_default.save();
        // }
        res.status(201).json(productsChanged)
        
    } catch (error) {
        res.status(400).json({message: error.message})
    }
})


// Decrease products of producers
router.post('/decrease/product', async (req, res) => {
    try {
        let products = req.body
        await Product_producer.findOne({_id: products.product_id})
        .populate({
            path: 'productDetailArr',
        })
        .exec(async function (error, element) {
            if (error) return res.status(400).json({message: error.message})
            let amount = products.amount
            for(let i = 0; i < element.productDetailArr.length; i++){
                let producer_product_detail = await Product_producer_detail.findById(element.productDetailArr[i].id)
                if(element.productDetailArr[i].amountLeft == 0){
                }else if(Number(products.amount) <= Number(element.productDetailArr[i].amountLeft)){
                    producer_product_detail.amountLeft = Number(producer_product_detail.amountLeft) - Number(amount)
                    await producer_product_detail.save();
                    console.log(producer_product_detail.amountLeft);
                    break;
                }else{
                    amount = Number(amount) - Number(producer_product_detail.amountLeft)
                    producer_product_detail.amountLeft = 0
                    await producer_product_detail.save();
                }
            }
            await element.save();
        });

        res.json('success')
    } catch (error) {
        res.status(400).json({message: error.message})
    }
})

// Add product return to producer from customer
router.post('/:cid/add/product/return/:pid', async (req, res) => {
    try {
        let product = await Product_customer.findById(req.params.pid)
        product.amount = product.amount - req.body.amount

        let productNew = await new Product_customer_return(req.body)
        await product.save();
        await productNew.save();
        res.json('success')
     
    } catch (error) {
        res.status(400).json({message: error.message})
    }
})

//Update one
router.patch('/:id', async (req, res) => {
    let customer = await Customer.findById(req.params.id);
    
    if(req.body.name != null){
        customer.name = req.body.name
    }
    if(req.body.phone != null){
        customer.phone = req.body.phone
    }
    if(req.body.companyName != null){
        customer.companyName = req.body.companyName
    }

    try {
        const updatedCustomer = await customer.save()
        res.json(updatedCustomer)
    } catch (error) {
        res.status(400).json({message: error.message})
    }
})

//Update one product of customer
router.patch('/:id/product/:pid', async (req, res) => {
    let product = await Product_customer.findById(req.params.pid);
    switch (true) { 
        case req.body.barcode != null:
            product.barcode = req.body.barcode
        case req.body.date != null:
            product.date = req.body.date
        case req.body.name != null:
            product.name = req.body.name
        case req.body.amount != null:
            product.amount = req.body.amount
        case req.body.measure != null:
            product.measure = req.body.measure
        case req.body.buyPrice != null:
            product.buyPrice = req.body.buyPrice
        case req.body.payed != null:
            product.payed = req.body.payed
        case req.body.unPayed != null:
            product.unPayed = req.body.unPayed
        case req.body.about != null:
            product.about = req.body.about
        // default:
            // res.status(400).json({message: 'error in switch'})
    }
    try {
        const updatedProduct = await product.save()
        res.json(updatedProduct)
    } catch (error) {
        res.status(400).json({message: error.message})
    }
})
//Update one returned product of customer
router.patch('/edit/return/product/:id', async (req, res) => {
    let product = await Product_customer_return.findById(req.params.id);
    switch (true) { 
        case req.body.amount != null:
            product.amount = req.body.amount
        case req.body.measure != null:
            product.measure = req.body.measure
        case req.body.buyPrice != null:
            product.buyPrice = req.body.buyPrice
        case req.body.returnReason != null:
            product.returnReason = req.body.returnReason
        // default:
            // res.status(400).json({message: 'error in switch'})
    }
    try {
        const updatedProduct = await product.save()
        res.json(updatedProduct)
    } catch (error) {
        res.status(400).json({message: error.message})
    }
})

//Update one item cart
router.patch('/update/item/cart/:id', async (req, res) => {
    let product = await Cart.findById(req.params.id);
    switch (true) { 
        case req.body.amount != null:
            product.amount = req.body.amount
        case req.body.buyPrice != null:
            product.buyPrice = req.body.buyPrice
        // default:
            // res.status(400).json({message: 'error in switch'})
    }
    try {
        const updatedProduct = await product.save()
        res.json(updatedProduct)
    } catch (error) {
        res.status(400).json({message: error.message})
    }
})

//Delete one item from cart
router.delete('/cart/delete/one', async (req, res) => {
    try {
        let item = await Cart.findById(req.body.id);
        await item.remove()
        res.json({message: 'Item deleted!'})
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})

//Delete all items in cart
router.delete('/cart/delete/all', async (req, res) => {
    let item = await Cart.findOne();
    try {
        await Cart.deleteMany({}, function (err) {
            if(err) console.log(err);
            console.log("Successful deletion");
          });
        res.json({message: 'Items deleted!', deleted: true})
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})

//Delete one customer
router.delete('/:id', async (req, res) => {
    let customer = await Customer.findById(req.params.id);
    try {
        await customer.remove()
        res.json({message: 'Customer deleted!', deleted: true})
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})

//Delete one product of customer
router.delete('/:id/product/:pid', async (req, res) => {
    try {
        let product = await Product_customer.findById(req.params.pid)

        await Customer.findById(req.params.id)
        .populate('productArr')
        .exec(async function (error, element) {
            if (error) return res.status(400).json({message: error.message})

            let arrayIndex = await element.productArr.findIndex(function( currentValue ) {
                return currentValue._id == req.params.pid; 
            })
            await product.remove();
            
            await element.productArr.splice(arrayIndex, 1);
            await element.save()
            res.json({message: 'Product of customer deleted!', deleted: true})
        });
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})
//Delete one returned product of customer
router.delete('/delete/return/product/:id', async (req, res) => {
    try {
        let product = await Product_customer_return.findById(req.params.id)
        await product.remove();
        res.json({message: 'Returned product of customer deleted!', deleted: true})
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})

async function getCustomer(req, res, next){
    // let customer;
    // try {
        // customer = await Customer.findById(req.params.id)
        // .populate('product')
        // .then(function (err, element) {
        //     if (err) return res.status(404).json({ message: 'Cannot find customer', error: err })
        //     console.log('The customer is %s', element);
        // });
        // if(customer == null){
        //     return res.status(404).json({ message: 'Cannot find customer' })
        // }
    // } catch (error) {
    //     return res.status(500).json({ message: error.message })   
    // }
    // res.customer = customer
    next()
}

module.exports = router