const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    barcode: String,
    name: String,
    product_id: String,
    amount: Number,
    measure: String,
    buyPrice: String,
    payed: String
})

module.exports = mongoose.model('cart', schema)
